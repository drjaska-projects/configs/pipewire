#!/bin/sh

set -eu

# {{{ old
#context.exec = [
#{
#	path = "pactl"
#	args = "load-module module-native-protocol-tcp listen=192.168.1.6 auth-anonymous=1"
#}
#]

# flags syntax maybe works? Guessing, docs I found didn't have flags...
#pulse.cmd = [
#{
#	cmd = "load-module"
#	args = "module-native-protocol-tcp"
#	flags = "listen=192.168.1.6 auth-anonymous=1"
#}
#]
# }}}

sudo mkdir -p /etc/pipewire/pipewire-pulse.conf.d/
cat <<EOF | sudo tee /etc/pipewire/pipewire-pulse.conf.d/50-network-party.conf
pulse.cmd = [
{
	cmd = "load-module"
	args = "module-native-protocol-tcp"
}
{
	cmd = "load-module"
	args = "module-zeroconf-publish"
}
]
EOF
