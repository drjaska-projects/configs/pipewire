#!/bin/sh

set -eu

sudo mkdir -p /etc/pipewire/pipewire-pulse.conf.d/
cat <<EOF | sudo tee /etc/pipewire/pipewire-pulse.conf.d/50-zeroconf-discover.conf
context.modules = [
{
	name = libpipewire-module-zeroconf-discover
	args = { }
}
]
EOF
